import Net (Net, createNet, trainNet, executeNet)
import NetDot
import Dot

import Control.Monad.Random
import Data.List

-- In this test, we train the net to do a xor operation

trainDatas::[([Double],[Double])]
trainDatas = take 30000 $ cycle  [([0,0], [0])
                                ,([1,0], [1])
                                ,([0,1], [1])
                                ,([1,1], [0])]

trainSample:: Net -> ([Double],[Double]) -> Net
trainSample net = uncurry (trainNet net)

main:: IO()
main = do
        net <- evalRandIO $ createNet [2,3,1]
        print $ length <$> net
        putStrLn "starting training"

        let trainednet = foldl trainSample net trainDatas
        putStrLn "Training done"
        --printVDot $ feedForward [1,1] trainednet
        putStrLn "Testing :"
        forM_ [[0,0],[0,1],[1,0],[1,1]] $ \input@[a,b] -> do
                                                      putStr $ show a ++ " xor " ++ show b ++ " = "
                                                      print $ executeNet trainednet input
