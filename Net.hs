module Net where

import Control.Monad.Random

-- | Contains the weight and the dweight
type Connection = (Double,Double)

-- | A neuron, its connection weights and it's output val, it's 
type Neuron = [Connection] 

-- | A valuated neuron
type VNeuron = (Neuron, Double)

--  | A vneuron with it's gradient
type ENeuron = (VNeuron, Double)

type Layer = [Neuron]
type VLayer = [VNeuron]
type ELayer = [ENeuron]

-- | A net of neurons
type Net = [Layer]

-- | A valuated net of neurons
type VNet = [VLayer]

type ENet = [ELayer]

-- | Overall learning rate [0 -> 1]
eta :: Double
eta = 0.15

-- | Momentum [0 -> 1]
alpha :: Double
alpha = 0.5

transferFunction :: Double -> Double
transferFunction = tanh

transferFunctionDerivative :: Double -> Double
transferFunctionDerivative x = 1 - (x*x)

createConnecton:: RandomGen g => Rand g Connection
createConnecton = flip (,) 0 <$> getRandomR (0,1)

createNeuron :: RandomGen g => Int -> Rand g Neuron
createNeuron n = replicateM n createConnecton 

-- | create A new net, if given [4,3], the network will have 2 layers, the first one has 4 Neuron and the second one 3
createNet:: RandomGen g =>  [Int] -> Rand g Net
createNet (x:xn:xs) = liftM2 (:) (replicateM (x+1) $ createNeuron xn) (createNet (xn:xs))
createNet [x] = (:[]) <$> replicateM (x+1) (createNeuron 0)
createNet [] = error "the net cannot be empty"

-- | Feed the values to the net assert the size of the list is the size of the first layer
feedForwardFst:: [Double] -> Layer -> VLayer
feedForwardFst v x = (uncurry (,) <$> zip x v) ++ [flip (,) 1 $ last x]

-- | feed the layer using the old layer and returning the new layer
feedForwardLayer :: VLayer -> Layer -> VLayer
feedForwardLayer old lay = addbias $ (\(i,n) -> (,) n $ transferFunction $ sum (neuronValNext i <$> old) ) <$> zip [0..] (init lay)
                where neuronValNext i (w,out) = out * fst(w!!i)
                      addbias = (++[flip (,) 1 $ last lay])

feedForward::[Double] -> Net -> VNet
feedForward d (x:xs) = scanl feedForwardLayer (feedForwardFst d x) xs
feedForward _ [] = error "the net cannot be empty"

getError:: [Double] -> VNet -> Double
getError expect net = sqrt $ (/lengthlast) $ sum $ (**2) . uncurry (-) <$> zip expect (snd <$> last net)
            where lengthlast = fromIntegral$length $ last net

-- | Calculate the gradient of the output nodes given the target values
calcGradientOut:: Double -> VNeuron -> ENeuron
calcGradientOut target n = (,) n $ (transferFunctionDerivative (snd n)) * (target - snd n)

-- | Calculate the gradient of the node based on the gradient of the next layer
calcGradientHidden:: [Double] -> VNeuron -> ENeuron
calcGradientHidden nextlayer n = (,)  n $ sumDow * (transferFunctionDerivative $ snd n)
                      where sumDow = sum $ dow <$> [0..(length nextlayer -2)]
                            dow i  = fst (fst n!!i) * (nextlayer!!i)

calcGradientsHiddenLayer:: VLayer -> ELayer -> ELayer
calcGradientsHiddenLayer l d = map (calcGradientHidden $ snd <$> d) l

calcGradientOutLayer:: [Double] -> VLayer -> ELayer
calcGradientOutLayer v l = (++[(last l, 0)]) $ uncurry calcGradientOut <$> zip v (init l)

calcGradients:: [Double] -> VNet -> ENet
calcGradients d n = scanr calcGradientsHiddenLayer (calcGradientOutLayer d  $ last n) $ init n

-- | update the weights of the connection between two neurons
deltaupdateWeightsNeuron:: ENeuron -> ENeuron -> Connection -> Connection
deltaupdateWeightsNeuron neuron to (weight, dweight) = (weight + newdweight, newdweight)
                                where newdweight = eta * neuronoutput * togradient -- Individual input
                                                   + alpha * dweight               -- Momentum
                                      neuronoutput = snd $ fst neuron
                                      togradient = snd $ to


updateWeightsNeuron:: ELayer -> ENeuron -> ENeuron
updateWeightsNeuron prev n@((conns,val),grad) = ((newconns, val), grad)
                              where newconns = map (\(pner,conn)-> deltaupdateWeightsNeuron n pner conn) $ zip prev conns
                              

-- | update the layer given the next layer and the current layer
updateWeighsLayer :: ELayer -> ELayer -> ELayer
updateWeighsLayer curr nextlayer = updateWeightsNeuron nextlayer <$> curr


-- | update the weights of the net
updateWeights:: ENet -> Net
updateWeights n = (map.map) stripvalues $ scanr updateWeighsLayer (last n) $ init n
                  where stripvalues = fst . fst

-- | backpropagation
backprop:: VNet -> [Double] -> Net
backprop net targets = updateWeights $ calcGradients targets net

-- | Get the results of a valuated net
getResults:: VNet -> [Double]
getResults n = init $ (snd <$> last n)

-- | Train new datas into the net
trainNet:: Net -> [Double] -> [Double] -> Net
trainNet net input expected = flip backprop expected $ feedForward input net

-- | Train a net and get the error (from 0 to 1)
trainNetError:: Net -> [Double] -> [Double] -> (Net,Double)
trainNetError net input expected = (,) (flip backprop expected fnet) $ sum $ fmap abs $ (-) <$> out <*> expected
                                    where fnet = feedForward input net
                                          out = getResults fnet
                                          
-- | Execute the net and get the output
executeNet:: Net -> [Double] -> [Double]
executeNet net input = getResults $ feedForward input net

