module NetDot where

import Dot
import Net
import Control.Monad

llindex:: [[a]] -> [(Int,Int,a)]
llindex m = concat $ (\(i,e) -> n i 0 e)<$> zip [0..] m
            where n x y (r:rs) = (x,y,r):n x (y+1)rs
                  n _ _ [] = []
fround:: (Fractional a, Integral b, RealFrac a1) => b -> a1 -> a
fround n f = fromInteger ( round $ f * (10^n)) / (10.0^^n)

netToDot:: Net -> DotGraph ()
netToDot n = forM_ (llindex n) shownode
              where nodename x y = "node"++show x++"_"++show y
                    shownode (x,y,node) = do
                                         addNode (nodename x y) $ if length (n!!x) == (y+1) then ["label=\"bias\"","color=blue"] else ["label=\"\""]
                                         forM_ (zip node [0..]) $ \((w,_),i) -> addLink (nodename x y) (nodename (x+1) (i::Int)) ["label=\""++(take 4 $show w)++"\""]

vnetToDot:: VNet -> DotGraph ()
vnetToDot n = forM_ (llindex n) shownode
              where nodename x y = "node"++show x++"_"++show y
                    shownode (x,y,node) = do
                                         addNode (nodename x y) $ if length (n!!x) == (y+1) then ["label=\"bias("++show (snd node)++")\"","color=blue"] else ["label=\""++ show ( fround 4 $snd node)++"\""]
                                         forM_ (zip (fst node) [0..]) $ \(w,i) -> addLink (nodename x y) (nodename (x+1) (i::Int)) ["label=\""++show (fround 4 $ fst $w)++"\""]

printDot:: String -> Net -> IO ()
printDot fname net = writeGraph fname (netToDot net)

printVDot:: String -> VNet -> IO ()
printVDot fname net = writeGraph fname (vnetToDot net)
