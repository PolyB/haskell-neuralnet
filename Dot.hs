module Dot where

import Data.Monoid

data DotGraph a = DotGraph String a

instance Functor DotGraph where
  fmap f (DotGraph s g) = DotGraph s (f g)

instance Applicative DotGraph where
  pure a = DotGraph "" a
  (DotGraph s f) <*> (DotGraph s2 v) = DotGraph (s++"\n"++s2) (f v)

instance Monad DotGraph where
  (DotGraph s v) >>= f = DotGraph (s++"\n"++str) c
                         where (DotGraph str c) = f v

addNode:: String -> [String] -> DotGraph ()
addNode n tags = flip DotGraph () $ (n<>) $ concat $ (\x->'[':x++"]") <$> tags

addLink:: String -> String -> [String] -> DotGraph ()
addLink from to tags = flip DotGraph () $ ((from++"->"++to)++) $ concat $ (\x->'[':x++"]") <$> tags

showGraph:: DotGraph a -> String
showGraph (DotGraph s _) = "Digraph G { graph [layout=dot rankdir=LR] \n" ++ s ++ "\n}"

writeGraph:: String -> DotGraph a -> IO()
writeGraph fname g = writeFile fname (showGraph g)

